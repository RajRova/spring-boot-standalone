import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PersonClient {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(PersonClient.class, args);
        PersonBean person = applicationContext.getBean(PersonBean.class);

        person.getPersonDetails();
    }
}
