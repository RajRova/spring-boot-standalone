package StandaloneSpringBoot.standalone_spring_boot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JFrame;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

import com.foo.Example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import Student.Student;
import view.MyWindowListener;
import view.Window;

@SpringBootApplication
public class PersonClient2 implements MyWindowListener {
	
	private final static String URI_STUDENTS = "http://localhost:8080/api/v1/student";
	 
	static RestTemplate restTemplate;
	
	public static void main(String[] args) {
		SpringApplicationBuilder builder = new SpringApplicationBuilder(PersonClient2.class);
		builder.headless(false);  // for standalone
		ConfigurableApplicationContext applicationContext = builder.run(args);
		
		PersonBean person = applicationContext.getBean(PersonBean.class);
		
		person.getPersonDetails();
		
		restTemplate = new RestTemplate();
		


		tryBeans();
		
		String result = restTemplate.getForObject(URI_STUDENTS, String.class);
		System.out.println(result); 


		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();        
		//Add the Jackson Message converter
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

		// Note: here we are making this converter to process any kind of response, 
		// not only application/*json, which is the default behaviour
		converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));        
		messageConverters.add(converter);  
		restTemplate.setMessageConverters(messageConverters);
		
		
		Student[] students = restTemplate.getForObject(URI_STUDENTS, Student[].class);
		System.out.println(students[0]); 
		
		PersonClient2 personClient2 = new PersonClient2();
		new Window().registerOnEventListener(personClient2);        
	}
	
	
	public static void tryBeans() {
		MessageSource resources = new ClassPathXmlApplicationContext("test/beans.xml");
	    String message = resources.getMessage("message", null, "Default", null);
	    System.out.println(message); // outputs : Alligators rock!

	    
	    ApplicationContext context = new ClassPathXmlApplicationContext("test/beans2.xml");
	    Example exm = (Example) context.getBean("example");
	    exm.execute();   // outputs :  The 'userDao' argument is required.
	    
	    message = resources.getMessage("argument.required",
	            new Object [] {"userDao"}, "Required", Locale.UK);
	    System.out.println(message);
	}


	@Override
	public void onClickEvent() {
		// TODO Auto-generated method stub
		System.out.println("this is in caller");
		Student[] students = restTemplate.getForObject(URI_STUDENTS, Student[].class);
		System.out.println(students[0]); 
	}
}
