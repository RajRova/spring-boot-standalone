package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Window extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JButton bouton;
	private MyWindowListener mListener;

	public Window() {
		setTitle("Test");
		setResizable(true);	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

		bouton=new JButton("click");//creating instance of JButton  
		bouton.setBounds(130,100,100, 40);//x axis, y axis, width, height  
		bouton.addActionListener(this);
		          
		JFrame f = this;
		f.add(bouton);//adding button in JFrame  
		          
		f.setSize(400,500);//400 width and 500 height  
		f.setLayout(null);//using no layout managers  
		f.setVisible(true);//making the frame visible  
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void registerOnEventListener(MyWindowListener mListener) {
		this.mListener = mListener;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(bouton)) {
			mListener.onClickEvent();
		}
	}
}
